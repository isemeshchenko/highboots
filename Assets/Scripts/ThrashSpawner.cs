﻿using System.Collections;
using System.Collections.Generic;
using AmphibianSpace;
using UnityEngine;

public class ThrashSpawner : Singleton<ThrashSpawner>
{
	public void SpawnThrash(ThrashTypeEnum type)
	{
		GameObject thrash =	Factory.Instance.GetThrash(type);

		if (thrash != null)
		{
			thrash.GetComponent<Thrash>().Init(type);
		}
		
	}
	
	
}
