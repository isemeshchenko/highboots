﻿using System.Collections;
using System.Collections.Generic;
using AmphibianSpace;
using UnityEngine;

namespace AmphibianSpace
{
	public class Factory : Singleton<Factory>
	{
		[HideInInspector] public DataPrefabs DataPrefabs;

		private void Awake()
		{
			DataPrefabs = GetComponent<DataPrefabs>();
		}

		public GameObject GetThrash(ThrashTypeEnum type)
		{
			return DataPrefabs.ThrashTypePrefabsDict[type];
		}

		public GameObject GetRandomThrash()
		{
			return DataPrefabs.ThrashTypePrefabsDict[(ThrashTypeEnum)Random.Range(0,4)];
		}

//		public GameObject GetCell(MatchValueEnum matchValue)
//		{
//			GameObject obj = Instantiate(DataPrefabs.CellPrefabDict[matchValue]);
//			return obj;
//		}
//
//		public GameObject GetRandomCell()
//		{
//			int randomIndex = Random.Range(0, 6);
//			return GetCell((MatchValueEnum) randomIndex);
//		}
//
//		public GameObject GetTile(TileTypeEnum tileType)
//		{
//			GameObject obj = Instantiate(DataPrefabs.TilePrefabDict[tileType]);
//			return obj;
//		}
//
//		public GameObject GetBomb(BombTypeEnum bombType)
//		{
//			GameObject obj = Instantiate(DataPrefabs.BombPrefabDict[bombType]);
//			return obj;
//		}
//
//		public GameObject GetBlocker(BlockerTypeEnum blockerType)
//		{
//			GameObject obj = Instantiate(DataPrefabs.BlockerPrefabDict[blockerType]);
//			return obj;
//		}
//
//		public GameObject GetCollectible()
//		{
//			int randomIndex = Random.Range(0, DataPrefabs.Collectibles.Count);
//			GameObject obj = Instantiate(DataPrefabs.Collectibles[randomIndex]);
//			return obj;
//		}
//	}
	}

}
