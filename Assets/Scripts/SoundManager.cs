﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public class SoundManager : Singleton<SoundManager>
    {
        public AudioSource Source;
	    public AudioClip Punch;
        public AudioClip Inhale;

        public void PlayPunch()
        {
            Source.PlayOneShot(Punch);
        }
        
        public void PlayInhale()
        {
            Source.PlayOneShot(Inhale);
        }


    }

}
