﻿using System.Collections;
using System.Collections.Generic;
using AmphibianSpace;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;
using UnityScript.Macros;


public class Generator : MonoBehaviour
{

	private float _offset = 0.5f;
	public bool IsPressed = false;
	public bool InputEnabled = true;
	public bool CanMove = true;
	
	
	public Vector3 TopMinPosition;
	public Vector3 TopMaxPosition;
	
	public Vector3 PalmMinPosition;
	public Vector3 PalmMaxPosition;

	public int CurrentPower;

	public RectTransform Top;
	public RectTransform Palm;
	
	
	public UnityEngine.UI.Slider Slider;

	public Text PowerText;

	public IEnumerator DisableInput(float delay)
	{
		InputEnabled = false;
		yield return new WaitForSeconds(delay);
		InputEnabled = true;
	}
	
	private void Awake()
	{
		Top.localPosition = TopMinPosition;
		Palm.localPosition = PalmMinPosition;

		StartCoroutine(DisableInput(3));
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space) && InputEnabled)
		{
			if (!IsPressed)
			{
				StartCoroutine(MoveOverSeconds(Top, TopMaxPosition, GameController.Instance.DataGame.GeneratorUpTime));
				StartCoroutine(MoveOverSeconds(Palm, PalmMaxPosition, GameController.Instance.DataGame.GeneratorUpTime));

				IsPressed = true;
			}
			else
			{
				print("select");
				StartCoroutine(Select());
			}
		}
		
		SetSliderValue();
		
		if (IsPressed)
		{
			SetPowerText();
			
		}
		
	}

	void SetPowerText()
	{
		PowerText.text = GetPower(Top.localPosition.y).ToString();
	}

	int GetPower(float currValue)
	{
		int result = (int)(currValue - TopMinPosition.y)/3;
		//print("Result value = " + result);
		
		return result;
	}


	void SetSliderValue()
	{
		Slider.value = GetPower(Top.localPosition.y);
	}
	
	


	



	public IEnumerator Select()
	{
		StartCoroutine(GameController.Instance.SetAnimationInhale());
		
		SoundManager.Instance.PlayInhale();
		
		GameController.Instance.HeadDown();
		
		
		
		CanMove = false;
//		StopCoroutine(MoveOverSeconds(Top, TopMaxPosition, GameController.Instance.DataGame.GeneratorUpTime));
//		StopCoroutine(MoveOverSeconds(Palm, PalmMaxPosition, GameController.Instance.DataGame.GeneratorUpTime));

		InputEnabled = false;
		
		CurrentPower = GetPower(Top.localPosition.y);

		if (CurrentPower > 100)
		{
			//УБИЛО
			StartCoroutine(DisableInput(3));
			Top.transform.DOPunchPosition(new Vector3(0, -50, 0), 1, 10);
			Palm.transform.DOPunchPosition(new Vector3(0, -50, 0), 1, 10);

			StartCoroutine(GameController.Instance.ShowWasted());
			
			yield return new WaitForSeconds(3);
		}
		else
		{
			yield return new WaitForSeconds(GameController.Instance.DataGame.SelectTime);
			Slider.value = CurrentPower;
		
			GameController.Instance.GetPower(CurrentPower);
		}
		
		
		

		CanMove = true;
		//GameController.Instance.SetAnimationExhale();
		StartCoroutine(MoveOverSeconds(Top, TopMinPosition, GameController.Instance.DataGame.GeneratorDownTime));
		StartCoroutine(MoveOverSeconds(Palm, PalmMinPosition, GameController.Instance.DataGame.GeneratorDownTime));
		
		yield return new WaitForSeconds(GameController.Instance.DataGame.GeneratorDownTime);

		IsPressed = false;
		InputEnabled = true;
		
		//GameController.Instance.SetAnimation(true);
		
		
	}
	
	
		
	public IEnumerator MoveOverSpeed (GameObject objectToMove, Vector3 end, float speed){
		// speed should be 1 unit per second
		while (objectToMove.transform.position != end)
		{
			objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, end, speed * Time.deltaTime);
			yield return new WaitForEndOfFrame ();
		}
	}
	public IEnumerator MoveOverSeconds (RectTransform objectToMove, Vector3 end, float seconds)
	{
		float elapsedTime = 0;
		Vector3 startingPos = objectToMove.localPosition;
		while (elapsedTime < seconds && CanMove )
		{
			objectToMove.localPosition = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();
			//objectToMove.localPosition = end;
		}
		
	}
	
	
}
