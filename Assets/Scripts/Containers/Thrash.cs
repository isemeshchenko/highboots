﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
	public class Thrash : Container
	{
		[HideInInspector] public DataHP DataHP;
		[HideInInspector] public DataThrashType DataThrashType;
		[HideInInspector] public DataView DataView;

		void Awake()
		{
			DataHP = Add<DataHP>();
			DataThrashType = Add<DataThrashType>();
			DataView = Add<DataView>();
		}

		public void Destroy()
		{
			GameController.Instance.DataGame.CurrentHP -= DataHP.CurrentHP;
			GameController.Instance.SetHPText();
			
			GameController.Instance.TakeDamage(DataHP.MaxHP);
			
			Destroy(gameObject);
			
			
		}

		public void SetNumber()
		{
			DataView.NumberText.text = DataHP.MaxHP.ToString();
		}
		
		public void SetNumber(int value)
		{
			DataView.NumberText.text = value.ToString();
		}


		public void Init(ThrashTypeEnum type)
		{
			DataThrashType.ThrashType = type;
			
			int number = 0;
			switch (type)
			{
				case ThrashTypeEnum.Thrash1:
				{
					number = Random.Range(GameController.Instance.DataGame.ThrashHPMin, GameController.Instance.DataGame.ThrashHPMid1);
					DataHP.MaxHP = number;
					break;
				}
				case ThrashTypeEnum.Thrash2:
				{
					number = Random.Range(GameController.Instance.DataGame.ThrashHPMid1, GameController.Instance.DataGame.ThrashHPMid2);
					DataHP.MaxHP = number;
					break;
				}
				case ThrashTypeEnum.Thrash3:
				{
					number = Random.Range(GameController.Instance.DataGame.ThrashHPMid2, GameController.Instance.DataGame.ThrashHPMid3);
					DataHP.MaxHP = number;
					break;
				}
				case ThrashTypeEnum.Thrash4:
				{
					number = Random.Range(GameController.Instance.DataGame.ThrashHPMid3, GameController.Instance.DataGame.ThrashHPMax);
					DataHP.MaxHP = number;
					break;
				}
			}
			//DataView.NumberText.text = number.ToString();
		}
		}
	}





