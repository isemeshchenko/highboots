﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataView : MonoBehaviour
    {
        public SpriteRenderer RendererSprite;
        public Text NumberText;

        private void Awake()
        {
            RendererSprite = GetComponent<SpriteRenderer>();
            NumberText = GetComponentInChildren<Text>();

            if (NumberText != null)
            {
               // print("NumberText != null");
            }
            else
            {
              //  print("NumberText null");
            }
        }
    }

}


