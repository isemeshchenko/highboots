﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataPrefabs : MonoBehaviour
    {
        public GameObject ThrashPrefab1;
        public GameObject ThrashPrefab2;
        public GameObject ThrashPrefab3;
        public GameObject ThrashPrefab4;

        public Dictionary<ThrashTypeEnum, GameObject> ThrashTypePrefabsDict = new Dictionary<ThrashTypeEnum, GameObject>();

        private void Awake()
        {
            ThrashTypePrefabsDict.Add(ThrashTypeEnum.Thrash1, ThrashPrefab1);
            ThrashTypePrefabsDict.Add(ThrashTypeEnum.Thrash2, ThrashPrefab2);
            ThrashTypePrefabsDict.Add(ThrashTypeEnum.Thrash3, ThrashPrefab3);
            ThrashTypePrefabsDict.Add(ThrashTypeEnum.Thrash4, ThrashPrefab4);
        }
    }

}


