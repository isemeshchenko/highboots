﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Comparers;
using UnityEngine.UI;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataGame : MonoBehaviour
    {
        public int ThrashHPMin;
        public int ThrashHPMid1;
        public int ThrashHPMid2;
        public int ThrashHPMid3;
        public int ThrashHPMax;
       

        public float GeneratorUpTime;
        public float GeneratorDownTime;
        public float SelectTime;
       // public float SpawnTime;
        public float MoveTime;


        public int MaxHP = 120;
        public int CurrentHP;

        public Text HPText;
        public Text MaxHPText;
    }

}


