﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataHP : MonoBehaviour
    {
        public int MaxHP;
        public int CurrentHP;
    }

}

