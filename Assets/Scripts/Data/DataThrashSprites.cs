﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    [System.Serializable]
    public class DataThrashSprites : MonoBehaviour
    {
        public Sprite Thrash1;
        public Sprite Thrash2;
        public Sprite Thrash3;
        public Sprite Thrash4;
      //  public Sprite Thrash5;

    }

}


