﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AmphibianSpace
{
    public enum ThrashTypeEnum
    {
        Thrash1,
        Thrash2,
        Thrash3,
        Thrash4
    }
    [System.Serializable]
    public class DataThrashType : MonoBehaviour
    {
        public ThrashTypeEnum ThrashType;
    }

}


