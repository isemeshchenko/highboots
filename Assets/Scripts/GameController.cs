﻿using System.Collections;

using AmphibianSpace;
using DG.Tweening;

using UnityEngine;
using UnityEngine.UI;

public class GameController : Singleton<GameController>
{
	[HideInInspector]	public DataGame DataGame;
	

	public Text DamageText;
	public Text StartContText;
	public GameObject GameOverPanel;
	public GameObject Robot;
	public Slider SliderHP;
	public GameObject WastedPanel;

	public GameObject Head;

	public AnimationClip animation;

	public Animator Animator;

	public void SetHPSlider(int hp)
	{
		if (hp >= 100)
		{
			hp = 100;
		}
		
		SliderHP.value = hp;
		
	}

	public void HeadDown()
	{
		Head.transform.DOPunchPosition(new Vector3(transform.position.x, transform.position.y - 2, 0), 3,0);
	}

	public IEnumerator ShowWasted()
	{
		WastedPanel.SetActive(true);
		yield return new WaitForSeconds(3);
		WastedPanel.SetActive(false);
	}
	
	IEnumerator StartDelay()
	{
		//yield return new WaitForSeconds(1);
		StartContText.gameObject.SetActive(true);
		yield return new WaitForSeconds(1);
		StartContText.text = "НА СТАРТ!";
		yield return new WaitForSeconds(1);
		StartContText.text = "УВАГА!";
		yield return new WaitForSeconds(1);
		StartContText.text = "ВПЕРЕД!";
		yield return new WaitForSeconds(1);
		StartContText.gameObject.SetActive(false);
	}

	public IEnumerator SetAnimationInhale()
	{
		Animator.SetTrigger("Inhale");
		yield return new WaitForSeconds(animation.length);
		Animator.SetTrigger("Exhale");
	}

	IEnumerator PlayPunch()
	{
		SoundManager.Instance.PlayPunch();
		yield return new WaitForSeconds(0.75f);
		StartCoroutine(PlayPunch());
	}

	private void Update()
	{
		SetHPSlider(DataGame.CurrentHP);
		
	}

	private void Awake()
	{
		DataGame = GetComponent<DataGame>();
		//DataGame.CurrentHP = DataGame.MaxHP;
		SetHPText();
		DamageText.gameObject.SetActive(false);
		StartContText.gameObject.SetActive(false);
		StartCoroutine(StartDelay());
		GameOverPanel.SetActive(false);
		WastedPanel.SetActive(false);
		StartCoroutine(PlayPunch());
		
//SetAnimation();

		//StartCoroutine(TakeDamageRoutine());
	}

	public void SetHPText()
	{
		//DataGame.HPText.text = "Curr HP " + DataGame.CurrentHP;
		DataGame.MaxHPText.text = "HP " + DataGame.CurrentHP;
	}

	public void GetPower(int powerValue)
	{
		DataGame.CurrentHP += powerValue;

//		if (DataGame.CurrentHP > DataGame.MaxHP)
//		{
//			int difference = DataGame.CurrentHP - DataGame.MaxHP;
//
//			DataGame.MaxHP -= difference;
//			DataGame.CurrentHP = DataGame.MaxHP;
//		}
		
		SetHPText();
	}

	public void TakeDamage(int damage)
	{
		DataGame.CurrentHP -= damage;
		
		if (DataGame.CurrentHP <= 0)
		{
			DataGame.CurrentHP = 0;
			//Game Over

			Time.timeScale = 0.1f;

			Generator generator = FindObjectOfType<Generator>();
			generator.InputEnabled = false;
			//generator.CanMove = false;
			ShowGameOver();
		}
		
		SetHPText();

		StartCoroutine(ShowDamageText(damage, 2f));
	}

	void ShowGameOver()
	{
		GameOverPanel.SetActive(true);
		//GameOverPanel.transform.D
	}
	
	IEnumerator TakeDamageRoutine()
	{
		yield return new WaitForSeconds(2);
		
		TakeDamage(10);

		StartCoroutine(TakeDamageRoutine());
	}

	IEnumerator ShowDamageText(int damage, float speed)
	{
		int randomX = Random.Range(-700, 0);
		int randomY = Random.Range(-100, 300);
		
		DamageText.gameObject.SetActive(true);
		DamageText.text = "-" + damage;
		
		DamageText.GetComponent<RectTransform>().localPosition = new Vector3(randomX, randomY, 0);

		//DamageText.gameObject.transform.DOPunchScale(new Vector3(1.5f, 1.5f, 0), speed, 3);
		DamageText.gameObject.transform.DOPunchPosition(new Vector3(20, 20, 0), speed, 3);
		
		yield return new WaitForSeconds(speed);
		
		DamageText.gameObject.SetActive(false);
		
	}
}
