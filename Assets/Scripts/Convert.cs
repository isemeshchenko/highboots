﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AmphibianSpace
{
    public class Convert : MonoBehaviour
    {
        public bool CanMove = true;
        public GameObject Garbage;
        
        public Vector3 StartPoint;
        public Vector3 Point0;
        public Vector3 Point1;
        public Vector3 Point2;
        public Vector3 Point3;
        public Vector3 Point4;
        public Vector3 Point5;
        
        [HideInInspector]
        public List<GameObject> ThrashList = new List<GameObject>();
        [HideInInspector]
        public GameObject[] ConvertPointObjects = new GameObject[6];
        
        private Dictionary<int, Vector3> IndexPointDict = new Dictionary<int, Vector3>();

        private void Awake()
        {
            IndexPointDict.Add(0, Point0);
            IndexPointDict.Add(1, Point1);
            IndexPointDict.Add(2, Point2);
            IndexPointDict.Add(3, Point3);
            IndexPointDict.Add(4, Point4);
            IndexPointDict.Add(5, Point5);
            
        }

        private void Start()
        {
            StartCoroutine(SpawnRoutine(GameController.Instance.DataGame.MoveTime));
        }

        void Spawn()
        {
            int random = UnityEngine.Random.Range(0, 3);
            
           // GameObject thrash = Instantiate(Factory.Instance.GetThrash((ThrashTypeEnum) UnityEngine.Random.Range(0,4) ));
            GameObject thrash = Instantiate(Factory.Instance.GetRandomThrash());
            
            RectTransform rect = thrash.GetComponent<RectTransform>();
            
            rect.SetParent(Garbage.transform, false);
            
            Thrash thr = thrash.GetComponent<Thrash>();
            thr.Init(thr.DataThrashType.ThrashType);

            rect.localPosition = StartPoint;

            ConvertPointObjects[0] = thrash;

            StartCoroutine(MoveOverSeconds(rect, Point0, GameController.Instance.DataGame.MoveTime));
        }

        

        IEnumerator MoveNext()
        {
            for (int i = ConvertPointObjects.Length-1; i >= 0; i--)
            {
                if (ConvertPointObjects[i] != null)
                {
                    StartCoroutine(MoveOverSeconds(ConvertPointObjects[i], IndexPointDict[i+1],
                        GameController.Instance.DataGame.MoveTime));
                    Thrash thrash = ConvertPointObjects[i].GetComponent<Thrash>();
                    
                    ConvertPointObjects[i] = thrash.gameObject;
                    
                    if (i != ConvertPointObjects.Length-1)
                    {
                        StartCoroutine(MoveOverSeconds(ConvertPointObjects[i], IndexPointDict[i+1],
                            GameController.Instance.DataGame.MoveTime));
                        
                       // yield return new WaitForSeconds(GameController.Instance.DataGame.MoveTime);

                        ConvertPointObjects[i + 1] = ConvertPointObjects[i];
                        ConvertPointObjects[i] = null;
                    }
                    else
                    {
                        ConvertPointObjects[i] = null;
                        yield return new WaitForSeconds(GameController.Instance.DataGame.MoveTime);
                        thrash.Destroy();
                        
                        
                    }
                    
                  //  thrash.SetNumber(i);
                }
                else
                {
                   // yield return new WaitForSeconds(GameController.Instance.DataGame.MoveTime);
                }
                
                
            }
            yield return new WaitForSeconds(0);
        }
        
        public IEnumerator MoveOverSeconds (RectTransform objectToMove, Vector3 end, float seconds)
        {
            float elapsedTime = 0;
            Vector3 startingPos = objectToMove.localPosition;
            while (elapsedTime < seconds)
            {
                objectToMove.localPosition = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            objectToMove.localPosition = end;
        }
        
        public IEnumerator MoveOverSeconds (GameObject objectMove, Vector3 end, float seconds)
        {
            if (objectMove != null)
            {
                RectTransform objectToMove = objectMove.GetComponent<RectTransform>();

                if (objectToMove != null)
                {
                    float elapsedTime = 0;
                    Vector3 startingPos = objectToMove.localPosition;
                    while (elapsedTime < seconds)
                    {
                       
                        objectToMove.localPosition = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
                        elapsedTime += Time.deltaTime;
                        yield return new WaitForEndOfFrame();
                        
                        
                    }
                    objectToMove.localPosition = end;
                }
                
                    
                
            }
            
        }

        IEnumerator SpawnRoutine(float spawnTime)
        {
            Spawn();
            yield return new WaitForSeconds(spawnTime);
            StartCoroutine(MoveNext());
            yield return new WaitForSeconds(GameController.Instance.DataGame.MoveTime);
            if (CanMove)
                StartCoroutine(SpawnRoutine(spawnTime));

            if (GameController.Instance.DataGame.MoveTime >= 0.1f)
            {
                GameController.Instance.DataGame.MoveTime -= 0.01f; 
            }
            
        }
    }
    

}


